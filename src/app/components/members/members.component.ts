import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ong-members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
})
export class MembersComponent implements OnInit {
  @Input() member: any;

  constructor() { }

  ngOnInit() { }

}
