import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-ong',
  templateUrl: './ong.component.html',
  styleUrls: ['./ong.component.scss'],
})
export class OngComponent implements OnInit {
  @Input() ongs: any;

  constructor() { }

  ngOnInit() { }

}
