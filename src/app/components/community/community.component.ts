import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-community',
  templateUrl: './community.component.html',
  styleUrls: ['./community.component.scss'],
})
export class CommunityComponent implements OnInit {
  @Input() communities: any;

  constructor() { }

  ngOnInit() { }

}
