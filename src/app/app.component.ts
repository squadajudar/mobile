import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { SharedService } from './services/shared.service'

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  public selectedIndex = 0;

  menu = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home',
      access: 0
    },
    {
      title: 'ONGs/OSCs',
      url: '/ongs',
      icon: 'earth',
      access: 0
    },
    {
      title: 'Comunidades',
      url: '/communities',
      icon: 'body',
      access: 0
    },
    {
      title: 'Minhas ONGs/OSCs',
      url: '/myongs',
      icon: 'earth',
      access: 1
    },
    {
      title: 'Minhas Comunidades',
      url: '/mycommunities',
      icon: 'body',
      access: 1
    },
    {
      title: 'Perfil',
      url: '/profile',
      icon: 'person-circle',
      access: 1
    },
    {
      title: 'Registre-se',
      url: '/register',
      icon: 'person-add',
      access: -1
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public shared: SharedService
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.shared.loadUser();
      this.splashScreen.hide();
    });
  }
}
