import { Injectable } from '@angular/core';
import { LoadingController, AlertController } from '@ionic/angular';
import { NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  currentUser: any;
  logo = '../../assets/images/arede-logo-branca.png';

  socialType = [
    {
      type: 'facebook'
    },
    {
      type: 'instagram'
    },
    {
      type: 'linkedin'
    },
    {
      type: 'twitter'
    },
    {
      type: 'youtube'
    },
    {
      type: 'site'
    },
  ]

  constructor(
    private loadingController: LoadingController,
    private alertController: AlertController,
    private navCtrl: NavController,
    private http: HttpClient,
    private router: Router
  ) { }

  back() {
    this.navCtrl.back();
  }

  async presentAlert(data: string) {
    const alert = await this.alertController.create({
      message: data,
      buttons: ['OK']
    });
    await alert.present();
  }

  async presentError(data) {
    if (data.error.message) {
      this.presentAlert(data.error.message);
    } else {
      this.presentAlert('Erro de conexão com o servidor');
    }
  }

  async presentLoading(data: string) {
    return await this.loadingController.create({
      message: data
    });
  }

  login(email: string, password: string) {
    try {
      return this.http.post(`${environment.api}/user/login`, {
        email,
        password
      });
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  register(user) {
    try {
      return this.http.post(`${environment.api}/user/new`, user);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  async logOut() {
    this.currentUser = null;
    await Storage.remove({ key: 'currentUser' });
    this.router.navigate(['/home'], { replaceUrl: true });
  }

  setUser(user) {
    this.currentUser = user;
    Storage.set({ key: 'currentUser', value: JSON.stringify(user) });
  }

  async loadUser() {
    const encodedUser = await Storage.get({ key: 'currentUser' });

    if (encodedUser.value !== null) {
      this.currentUser = JSON.parse(encodedUser.value);
    }
  }

  testeEmail(email) {
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
  }

  validaConfirmEmail(email, confirm) {
    if (email !== confirm) {
      this.presentAlert('Os e-mails não conferem');
      return false;
    }
    return true;
  }

  navigate(page, params = null) {
    if (params) {
      this.router.navigate([page], params);
    } else {
      this.router.navigate([page]);
    }
  }

  findCep(cep: string) {
    try {
      return this.http.get(`${environment.cep}/ws/${cep}/json`);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  findCoordinates(location: string) {
    try {
      return this.http.get(`${environment.map}/search?format=json&limit=1&q=${location}`);
    } catch (e) {
      console.log(e);
      return e;
    }
  }
}
