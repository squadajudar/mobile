import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  updateBio(newBio: string, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/user/updateBio`, { newBio }, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  updatePrivacy(privacy: boolean, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/user/updatePrivacy`, { privacy }, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  updatePass(security: object, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/user/updatePass`, security, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }
}
