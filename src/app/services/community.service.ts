import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommunityService {
  loadedCommunity: any;
  communityMember: any;

  constructor(private http: HttpClient) { }

  getCommunitiesList() {
    try {
      return this.http.get(`${environment.api}/communities`);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  getMyCommunitiesList(token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.get(`${environment.api}/mycommunities`, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  getCommunity(id: string) {
    try {
      return this.http.get(`${environment.api}/community/${id}`);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  create(community, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/communities/new`, community, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  addMember(community, member, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/community/${community}/members/add/`, member, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  updateAddress(address: object, community: string, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/community/${community}/updateAddress/`, address, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  setSocial(social: object, community: string, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/community/${community}/social/add/`, social, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  updateMember(member: object, community: string, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/community/${community}/members/add/`, member, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  applyMember(user: string, community: string) {
    try {
      return this.http.post(`${environment.api}/community/${community}/members/apply/`, { user });
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  transferAuthority(user: object, community: string, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/community/${community}/authorityTransfer`, user, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }
}
