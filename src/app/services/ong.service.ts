import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OngService {
  loadedOng: any;
  ongMember: any;

  constructor(private http: HttpClient) { }

  getOngsList() {
    try {
      return this.http.get(`${environment.api}/ongs`);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  getMyOngsList(token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.get(`${environment.api}/myongs`, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  getOng(id: string) {
    try {
      return this.http.get(`${environment.api}/ong/${id}`);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  create(ong, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/ongs/new`, ong, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  addMember(ong, member, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/ong/${ong}/members/add/`, member, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  updateAddress(address: object, ong: string, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/ong/${ong}/updateAddress/`, address, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  setSocial(social: object, ong: string, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/ong/${ong}/social/add/`, social, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  // removeSocial(social: object) {
  //   console.log(this.loadedOng.social);
  //   const position = this.loadedOng.social.indexOf(social, 0);
  //   if (position > -1) {
  //     this.loadedOng.social.splice(position, 1);
  //   }
  //   console.log(this.loadedOng.social);
  // }

  updateMember(member: object, ong: string, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/ong/${ong}/members/add/`, member, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  applyMember(user: string, ong: string) {
    try {
      return this.http.post(`${environment.api}/ong/${ong}/members/apply/`, { user });
    } catch (e) {
      console.log(e);
      return e;
    }
  }

  transferAuthority(user: object, ong: string, token: string) {
    try {
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: token
        })
      };
      return this.http.post(`${environment.api}/ong/${ong}/authorityTransfer`, user, httpOptions);
    } catch (e) {
      console.log(e);
      return e;
    }
  }
}
