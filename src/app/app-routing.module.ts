import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'ongs',
    loadChildren: () => import('./pages/ongs/ongs.module').then(m => m.OngsPageModule)
  },
  {
    path: 'ong',
    loadChildren: () => import('./pages/ongs-detail/ongs-detail.module').then(m => m.OngsDetailPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterPageModule)
  },
  {
    path: 'communities',
    loadChildren: () => import('./pages/communities/communities.module').then(m => m.CommunitiesPageModule)
  },
  {
    path: 'mycommunities',
    loadChildren: () => import('./pages/mycommunities/mycommunities.module').then(m => m.MycommunitiesPageModule)
  },
  {
    path: 'myongs',
    loadChildren: () => import('./pages/myongs/myongs.module').then(m => m.MyongsPageModule)
  },
  {
    path: 'create-ong',
    loadChildren: () => import('./pages/create-ong/create-ong.module').then(m => m.CreateOngPageModule)
  },
  {
    path: 'create-community',
    loadChildren: () => import('./pages/create-community/create-community.module').then(m => m.CreateCommunityPageModule)
  },
  {
    path: 'community',
    loadChildren: () => import('./pages/communities-detail/communities-detail.module').then(m => m.CommunitiesDetailPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./pages/profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'members',
    loadChildren: () => import('./pages/community-tabs/members/members.module').then( m => m.MembersPageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./pages/community-tabs/contact/contact.module').then( m => m.ContactPageModule)
  },
  {
    path: 'config',
    loadChildren: () => import('./pages/community-tabs/config/config.module').then( m => m.ConfigPageModule)
  },
  {
    path: 'about',
    loadChildren: () => import('./pages/community-tabs/about/about.module').then( m => m.AboutPageModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
