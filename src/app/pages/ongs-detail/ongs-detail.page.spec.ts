import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OngsDetailPage } from './ongs-detail.page';

describe('OngsDetailPage', () => {
  let component: OngsDetailPage;
  let fixture: ComponentFixture<OngsDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OngsDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OngsDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
