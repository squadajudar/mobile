import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { SharedService } from '../../services/shared.service';
import { OngService } from '../../services/ong.service';

@Component({
  selector: 'app-ongs-detail',
  templateUrl: './ongs-detail.page.html',
  styleUrls: ['./ongs-detail.page.scss'],
})
export class OngsDetailPage implements OnInit {
  ong: any;

  constructor(
    public shared: SharedService,
    private route: ActivatedRoute,
    public ongService: OngService
  ) { }

  ngOnInit() {
    this.ongService.loadedOng = null;
    this.route.params.subscribe(params => {
      if (params.id) {
        this.getOng(params.id);
      } else {
        this.shared.back();
      }
    });
  }

  getOng(ongId) {
    this.ongService.getOng(ongId).subscribe(
      success => {
        const ong = success;
        ong.members.sort().sort(
          (a, b) => b.access - a.access
        );
        this.ongService.loadedOng = ong;
        this.ongService.ongMember = ong.members.find(member => member.user._id === this.shared.currentUser._id);
      },
      () => {
        this.shared.back();
      }
    )
  }
}
