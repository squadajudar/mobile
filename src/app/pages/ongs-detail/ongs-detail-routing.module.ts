import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OngsDetailPage } from './ongs-detail.page';

const routes: Routes = [
  {
    path: ':id',
    component: OngsDetailPage,
    children: [
      {
        path: 'about',
        children: [
          {
            path: '',
            loadChildren: () => import('../ong-tabs/about/about.module').then(m => m.AboutPageModule)
          }
        ]
      },
      {
        path: 'contact',
        children: [
          {
            path: '',
            loadChildren: () => import('../ong-tabs/contact/contact.module').then(m => m.ContactPageModule)
          }
        ]
      },
      {
        path: 'members',
        children: [
          {
            path: '',
            loadChildren: () => import('../ong-tabs/members/members.module').then(m => m.MembersPageModule)
          }
        ]
      },
      {
        path: 'config',
        children: [
          {
            path: '',
            loadChildren: () => import('../ong-tabs/config/config.module').then(m => m.ConfigPageModule),
          }
        ]
      },
      {
        path: '',
        redirectTo: 'about',
        pathMatch: 'full',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OngsDetailPageRoutingModule { }
