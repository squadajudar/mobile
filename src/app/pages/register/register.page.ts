import { Component, OnInit } from '@angular/core';

import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  register = {
    name: '',
    email: '',
    confirmEmail: '',
    password: '',
    confirmPassword: ''
  };

  constructor(
    public shared: SharedService
  ) { }

  ngOnInit() {
  }

  async validateRegister() {
    const loading = await this.shared.presentLoading('Verificando dados...');
    loading.present();

    if (!this.register.name) {
      this.shared.presentAlert('Informe nome e sobrenome');
      loading.dismiss();
      return false;
    }

    if (!this.register.email) {
      this.shared.presentAlert('Informe o email');
      loading.dismiss();
      return false;
    }

    if (!this.shared.testeEmail(this.register.email)) {
      this.shared.presentAlert('E-mail inválido');
      loading.dismiss();
      return false;
    }

    if (!this.register.confirmEmail) {
      this.shared.presentAlert('Confirme o email');
      loading.dismiss();
      return false;
    }

    if (!this.shared.validaConfirmEmail(this.register.email, this.register.confirmEmail)) {
      this.register.confirmEmail = '';
      loading.dismiss();
      return false;
    }

    if (!this.register.password) {
      this.shared.presentAlert('Informe a senha');
      loading.dismiss();
      return false;
    }

    if (!this.register.confirmPassword) {
      this.shared.presentAlert('Confirme a senha');
      loading.dismiss();
      return false;
    }

    if (this.register.password !== this.register.confirmPassword) {
      this.shared.presentAlert('As senhas não conferem');
      loading.dismiss();
      return false;
    }

    this.shared.register(this.register).subscribe(
      success => {
        this.shared.presentAlert(success.message);
        this.shared.navigate('/home');
        this.register = {
          name: '',
          email: '',
          confirmEmail: '',
          password: '',
          confirmPassword: ''
        };
        loading.dismiss();
      },
      error => {
        this.shared.presentError(error);
        loading.dismiss();
      }
    )
  }

}
