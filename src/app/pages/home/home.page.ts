import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { SharedService } from '../../services/shared.service';

import { LoginComponent } from '../modal/login/login.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    public shared: SharedService,
    public modalController: ModalController
  ) { }

  ngOnInit() {
  }

  async login() {
    const modal = await this.modalController.create({
      component: LoginComponent
    });

    return await modal.present();
  }

  goToDevs() {
    window.open('http://gabrielhenriq.com.br/efs/', '_system', 'location=yes');
  }

}
