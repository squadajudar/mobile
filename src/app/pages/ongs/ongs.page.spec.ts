import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { OngsPage } from './ongs.page';

describe('OngsPage', () => {
  let component: OngsPage;
  let fixture: ComponentFixture<OngsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OngsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(OngsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
