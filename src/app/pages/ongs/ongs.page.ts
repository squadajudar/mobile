import { Component, OnInit } from '@angular/core';

import { SharedService } from '../../services/shared.service';
import { OngService } from '../../services/ong.service';

@Component({
  selector: 'app-ongs',
  templateUrl: './ongs.page.html',
  styleUrls: ['./ongs.page.scss'],
})
export class OngsPage implements OnInit {
  ongs: any;
  filteredOngs: any
  search: any;

  constructor(
    public shared: SharedService,
    private ongService: OngService
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.ongService.getOngsList().subscribe(
      success => {
        this.ongs = success;
        this.filteredOngs = success;
      }
    );
  }

  filterOscs() {
    this.filteredOngs = this.ongs.filter(
      osc => osc.name.toLowerCase().includes(this.search.toLowerCase())
    );
  }

}
