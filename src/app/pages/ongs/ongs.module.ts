import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OngsPageRoutingModule } from './ongs-routing.module';

import { OngsPage } from './ongs.page';

import { HeaderComponent } from '../../components/header/header.component';
import { OngComponent } from '../../components/ong/ong.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    OngsPageRoutingModule,
  ],
  declarations: [OngsPage, HeaderComponent, OngComponent]
})
export class OngsPageModule { }
