import { Component, OnInit } from '@angular/core';
import { OngService } from '../../../services/ong.service';

import { SharedService } from '../../../services/shared.service';

@Component({
  selector: 'app-members',
  templateUrl: './members.page.html',
  styleUrls: ['./members.page.scss'],
})
export class MembersPage implements OnInit {
  memberList: any;

  constructor(
    public ongService: OngService,
    public shared: SharedService
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.memberList = this.ongService.loadedOng.members
  }

  apply() {
    this.ongService.applyMember(this.shared.currentUser._id, this.ongService.loadedOng._id).subscribe(
      success => {
        const { ong } = success;
        ong.members.sort().sort(
          (a, b) => b.access - a.access
        );
        this.ongService.loadedOng = ong;
        this.memberList = ong.members;
        this.ongService.ongMember = ong.members.find(member => member.user._id === this.shared.currentUser._id);
        this.shared.presentAlert(success.message);
      }
    )
  }

}
