import { Component, OnInit } from '@angular/core';

import { OngService } from '../../../services/ong.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  constructor(
    public ongService: OngService,
  ) { }

  ngOnInit() { }
}
