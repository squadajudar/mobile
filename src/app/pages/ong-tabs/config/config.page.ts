import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { OngService } from '../../../services/ong.service';
import { SharedService } from '../../../services/shared.service';

import { Map, tileLayer, marker } from 'leaflet';

@Component({
  selector: 'app-config',
  templateUrl: './config.page.html',
  styleUrls: ['./config.page.scss'],
})
export class ConfigPage implements OnInit {
  options = {
    address: false,
    social: false,
    socialEdit: false,
    members: false,
    authority: false
  }

  memberList: any;

  address = {
    cep: '',
    logradouro: '',
    numero: '',
    complemento: '',
    bairro: '',
    localidade: '',
    uf: '',
    latitude: 0,
    longitude: 0
  }

  socialToAdd = {
    name: '',
    link: ''
  }

  memberToEdit = {
    id: '',
    access: ''
  }

  memberToTransfer = {
    id: ''
  }

  searchAlert = false;

  map: Map;
  newMarker: any;
  showMap = false;

  constructor(
    public ongService: OngService,
    public shared: SharedService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    const { address } = this.ongService?.loadedOng
    if (address) {
      this.address = address;
    }
    this.filterMembers();
  }

  filterMembers() {
    this.memberList = this.ongService.loadedOng.members.filter(
      member => member._id !== this.ongService.ongMember._id && member.access < this.ongService.ongMember.access
    )
  }

  ionViewDidLeave() {
    this.showMap = false;
    this.options = {
      address: false,
      social: false,
      socialEdit: false,
      members: false,
      authority: false
    }
  }

  openConfig(group: string) {
    this.options[group] = !this.options[group];

    if (group === 'address' && this.options.address === true && this.address.latitude !== 0 && this.address.longitude !== 0) {
      this.showMap = true;
      setTimeout(() => {
        this.loadMap(this.address.latitude, this.address.longitude);
      }, 1000);
    } else if (group === 'address' && this.options.address === false) {
      this.showMap = false;
    }
  }

  findCep() {
    this.showMap = false;
    if (this.address.cep.length === 8 || this.address.cep.length === 9) {
      this.searchAlert = true
      this.address.logradouro = 'Buscando...';
      this.address.bairro = 'Buscando...';
      this.address.localidade = 'Buscando...';
      this.address.uf = 'Buscando...';
      this.shared.findCep(this.address.cep).subscribe(
        success => {
          const { logradouro, bairro, localidade, uf } = success
          this.address.logradouro = logradouro;
          this.address.bairro = bairro;
          this.address.localidade = localidade;
          this.address.uf = uf;
          this.searchAlert = false;
          this.shared.findCoordinates(`${logradouro} ${bairro} ${localidade} ${uf}`).subscribe(
            location => {
              if (location[0]) {
                this.showMap = true;
                const { lat, lon } = location[0];
                setTimeout(() => {
                  this.loadMap(lat, lon);
                }, 1000);
              }
            }
          )
        },
        () => {
          this.address.logradouro = '';
          this.address.bairro = '';
          this.address.localidade = '';
          this.address.uf = '';
          this.searchAlert = false;
        }
      );
    }
  }

  getMarkerLatLon() {
    console.log(this.newMarker.getLatLng());
  }

  loadMap(latitude, longitude) {
    this.map = new Map('mapaloc').setView([latitude, longitude], 15);
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      { attribution: 'Map data © <a href="https://www.openstreetmap.org/">OpenStreetMap</a>' }
    ).addTo(this.map);
    this.newMarker = marker([latitude, longitude], { draggable: true }).addTo(this.map);
  }

  updateAddress() {
    const { lat: latitude, lng: longitude } = this.newMarker.getLatLng();
    this.address.latitude = latitude;
    this.address.longitude = longitude;
    this.ongService.updateAddress(this.address, this.ongService.loadedOng._id, this.shared.currentUser.token).subscribe(
      success => {
        this.ongService.loadedOng = success.ong;
        this.address = success.ong.address;
        this.shared.presentAlert(success.message);
        this.openConfig('address');
      },
      error => {
        this.shared.presentError(error)
      }
    )
  }

  setSocial() {
    this.ongService.setSocial(this.socialToAdd, this.ongService.loadedOng._id, this.shared.currentUser.token).subscribe(
      success => {
        this.ongService.loadedOng = success.ong;
        this.socialToAdd = {
          name: '',
          link: ''
        };
        this.shared.presentAlert(success.message);
        this.openConfig('social');
      },
      error => {
        this.shared.presentError(error)
      }
    )
  }

  updateSocial() {
    // this.ongService.setSocial(this.socialToAdd, this.ongService.loadedOng._id, this.shared.currentUser.token).subscribe(
    //   success => {
    //     this.ongService.loadedOng = success.ong;
    //     this.socialToAdd = {
    //       name: '',
    //       link: ''
    //     };
    //     this.shared.presentAlert(success.message);
    //     this.openConfig('socialEdit');
    //   },
    //   error => {
    //     this.shared.presentError(error)
    //   }
    // )
    this.openConfig('socialEdit');
  }

  updateMember() {
    this.ongService.updateMember(this.memberToEdit, this.ongService.loadedOng._id, this.shared.currentUser.token).subscribe(
      success => {
        const { ong } = success;
        ong.members.sort().sort(
          (a, b) => b.access - a.access
        );
        this.ongService.loadedOng = ong;
        this.memberToEdit = {
          id: '',
          access: ''
        };
        this.filterMembers();
        this.shared.presentAlert(success.message);
        this.openConfig('members');
      },
      error => {
        this.shared.presentError(error)
      }
    )
  }

  transferAutority() {
    if (!this.memberToTransfer.id) {
      this.shared.presentAlert('Selecione um membr');
      return false
    }

    this.ongService.transferAuthority(this.memberToTransfer, this.ongService.loadedOng._id, this.shared.currentUser.token).subscribe(
      success => {
        const { ong } = success;
        ong.members.sort().sort(
          (a, b) => b.access - a.access
        );
        this.ongService.loadedOng = ong;
        this.memberToTransfer = {
          id: ''
        };
        this.ongService.ongMember = ong.members.find(member => member.user._id === this.shared.currentUser._id);
        this.filterMembers();
        this.shared.presentAlert(success.message);
        this.router.navigate([`/ong/${ong._id}`], { replaceUrl: true });
        this.openConfig('authority');
      }
    )
  }

}
