import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CreateOngPage } from './create-ong.page';

describe('CreateOngPage', () => {
  let component: CreateOngPage;
  let fixture: ComponentFixture<CreateOngPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateOngPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CreateOngPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
