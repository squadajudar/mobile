import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateOngPageRoutingModule } from './create-ong-routing.module';

import { CreateOngPage } from './create-ong.page';

import { HeaderComponent } from '../../components/header/header.component';

import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateOngPageRoutingModule,
    BrMaskerModule
  ],
  declarations: [CreateOngPage, HeaderComponent]
})
export class CreateOngPageModule { }
