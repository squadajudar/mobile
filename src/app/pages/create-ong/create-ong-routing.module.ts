import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateOngPage } from './create-ong.page';

const routes: Routes = [
  {
    path: '',
    component: CreateOngPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateOngPageRoutingModule {}
