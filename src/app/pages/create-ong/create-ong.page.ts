import { Component, OnInit } from '@angular/core';

import { SharedService } from '../../services/shared.service';
import { OngService } from '../../services/ong.service';

@Component({
  selector: 'app-create-ong',
  templateUrl: './create-ong.page.html',
  styleUrls: ['./create-ong.page.scss'],
})
export class CreateOngPage implements OnInit {
  ong = {
    name: '',
    email: '',
    phone: '',
    description: ''
  }

  constructor(
    public shared: SharedService,
    private ongService: OngService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.ongService.create(this.ong, this.shared.currentUser.token).subscribe(
      success => {
        this.shared.presentAlert(success.message);
        this.ong = {
          name: '',
          email: '',
          phone: '',
          description: ''
        }
        this.shared.navigate(`/ong/${success.ong._id}`);
      },
      error => {
        this.shared.presentError(error)
      }
    )
  }

}
