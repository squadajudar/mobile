import { Component, OnInit } from '@angular/core';

import { CommunityService } from '../../../services/community.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {

  constructor(
    public communityService: CommunityService
  ) { }

  ngOnInit() {
  }

}
