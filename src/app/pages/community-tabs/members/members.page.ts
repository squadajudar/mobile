import { Component, OnInit } from '@angular/core';
import { CommunityService } from '../../../services/community.service';

import { SharedService } from '../../../services/shared.service';

@Component({
  selector: 'app-members',
  templateUrl: './members.page.html',
  styleUrls: ['./members.page.scss'],
})
export class MembersPage implements OnInit {
  memberList: any;

  constructor(
    public communityService: CommunityService,
    public shared: SharedService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.memberList = this.communityService.loadedCommunity.members
  }

  apply() {
    this.communityService.applyMember(this.shared.currentUser._id, this.communityService.loadedCommunity._id).subscribe(
      success => {
        const { community } = success;
        community.members.sort().sort(
          (a, b) => b.access - a.access
        );
        this.communityService.loadedCommunity = community;
        this.memberList = community.members;
        this.communityService.communityMember = community.members.find(member => member.user._id === this.shared.currentUser._id);
        this.shared.presentAlert(success.message);
      }
    )
  }

}
