import { Component, OnInit } from '@angular/core';

import { CommunityService } from '../../../services/community.service';

import { Map, tileLayer, marker } from 'leaflet';

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  map: Map;
  newMarker: any;

  showMap = false;

  constructor(
    public communityService: CommunityService
  ) { }

  ngOnInit() {
  }

  ionViewWillEnter() {
    setTimeout(() => {
      if (this.communityService?.loadedCommunity?.address) {
        const { latitude, longitude } = this.communityService?.loadedCommunity?.address
        if (latitude && longitude) {
          this.showMap = true;
          setTimeout(() => {
            this.loadMap(latitude, longitude);
          }, 1000);
        }
      }
    }, 1000)
  }

  ionViewDidLeave() {
    this.showMap = false;
  }

  loadMap(latitude: number, longitude: number) {
    this.map = new Map('mapa').setView([latitude, longitude], 15);
    tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      { attribution: 'Map data © <a href="https://www.openstreetmap.org/">OpenStreetMap</a>' }
    ).addTo(this.map);
    this.newMarker = marker([latitude, longitude], { draggable: false }).addTo(this.map);
  }

}
