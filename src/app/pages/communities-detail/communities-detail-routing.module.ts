import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CommunitiesDetailPage } from './communities-detail.page';

const routes: Routes = [
  {
    path: ':id',
    component: CommunitiesDetailPage,
    children: [
      {
        path: 'about',
        children: [
          {
            path: '',
            loadChildren: () => import('../community-tabs/about/about.module').then(m => m.AboutPageModule)
          }
        ]
      },
      {
        path: 'contact',
        children: [
          {
            path: '',
            loadChildren: () => import('../community-tabs/contact/contact.module').then(m => m.ContactPageModule)
          }
        ]
      },
      {
        path: 'members',
        children: [
          {
            path: '',
            loadChildren: () => import('../community-tabs/members/members.module').then(m => m.MembersPageModule)
          }
        ]
      },
      {
        path: 'config',
        children: [
          {
            path: '',
            loadChildren: () => import('../community-tabs/config/config.module').then(m => m.ConfigPageModule),
          }
        ]
      },
      {
        path: '',
        redirectTo: 'about',
        pathMatch: 'full',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CommunitiesDetailPageRoutingModule { }
