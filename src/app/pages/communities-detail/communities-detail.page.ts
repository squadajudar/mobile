import { Component, OnInit } from '@angular/core';
import { CommunityService } from '../../services/community.service';
import { ActivatedRoute } from '@angular/router';

import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-communities-detail',
  templateUrl: './communities-detail.page.html',
  styleUrls: ['./communities-detail.page.scss'],
})
export class CommunitiesDetailPage implements OnInit {

  constructor(
    public shared: SharedService,
    private route: ActivatedRoute,
    public communityService: CommunityService
  ) { }

  ngOnInit() {
    this.communityService.loadedCommunity = null;
    this.route.params.subscribe(params => {
      if (params.id) {
        this.getCommunity(params.id);
      } else {
        this.shared.back();
      }
    });
  }

  getCommunity(communityId) {
    this.communityService.getCommunity(communityId).subscribe(
      success => {
        const community = success
        community.members.sort().sort(
          (a, b) => b.access - a.access
        );
        this.communityService.loadedCommunity = community;
        this.communityService.communityMember = community.members.find(member => member.user._id === this.shared.currentUser._id);
        console.log(this.communityService.communityMember)
      },
      () => {
        this.shared.back();
      }
    )
  }

}
