import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CommunitiesDetailPageRoutingModule } from './communities-detail-routing.module';

import { CommunitiesDetailPage } from './communities-detail.page';

import { HeaderComponent } from '../../components/header/header.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommunitiesDetailPageRoutingModule
  ],
  declarations: [CommunitiesDetailPage, HeaderComponent]
})
export class CommunitiesDetailPageModule { }
