import { Component, OnInit } from '@angular/core';

import { SharedService } from '../../services/shared.service';
import { CommunityService } from '../../services/community.service';

@Component({
  selector: 'app-communities',
  templateUrl: './communities.page.html',
  styleUrls: ['./communities.page.scss'],
})
export class CommunitiesPage implements OnInit {
  communities: any;
  filteredCommunities: any
  search: any;

  constructor(
    public shared: SharedService,
    private communityService: CommunityService
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.communityService.getCommunitiesList().subscribe(
      success => {
        this.communities = success;
        this.filteredCommunities = success;
      }
    );
  }

  filterCommunity() {
    this.filteredCommunities = this.communities.filter(
      community => community.name.toLowerCase().includes(this.search.toLowerCase())
    );
  }

}
