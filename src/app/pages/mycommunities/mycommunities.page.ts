import { Component, OnInit } from '@angular/core';

import { SharedService } from '../../services/shared.service';
import { CommunityService } from '../../services/community.service';

@Component({
  selector: 'app-mycommunities',
  templateUrl: './mycommunities.page.html',
  styleUrls: ['./mycommunities.page.scss'],
})
export class MycommunitiesPage implements OnInit {
  communities: any;
  filteredCommunities: any
  search: any;

  constructor(
    public shared: SharedService,
    private communityService: CommunityService
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.communityService.getMyCommunitiesList(this.shared.currentUser.token).subscribe(
      success => {
        this.communities = success;
        this.filteredCommunities = success;
      }
    );
  }

  filterCommunity() {
    this.filteredCommunities = this.communities.filter(
      community => community.name.toLowerCase().includes(this.search.toLowerCase())
    );
  }

}
