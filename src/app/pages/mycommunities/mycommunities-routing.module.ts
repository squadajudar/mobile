import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MycommunitiesPage } from './mycommunities.page';

const routes: Routes = [
  {
    path: '',
    component: MycommunitiesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MycommunitiesPageRoutingModule {}
