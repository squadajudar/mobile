import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MycommunitiesPageRoutingModule } from './mycommunities-routing.module';

import { MycommunitiesPage } from './mycommunities.page';

import { HeaderComponent } from '../../components/header/header.component';
import { CommunityComponent } from '../../components/community/community.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MycommunitiesPageRoutingModule
  ],
  declarations: [MycommunitiesPage, HeaderComponent, CommunityComponent]
})
export class MycommunitiesPageModule { }
