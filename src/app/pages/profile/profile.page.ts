import { Component, OnInit } from '@angular/core';

import { SharedService } from '../../services/shared.service';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  options = {
    bio: false,
    email: false,
    privacy: false,
    security: false
  }

  newBio: string;
  security: ChangePass;
  privacy = false;

  constructor(
    public shared: SharedService,
    public userService: UserService,
  ) {
    this.security = {
      oldPass: '',
      newPass: '',
      confirmNewPass: ''
    }
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.newBio = this.shared.currentUser.about;
    this.privacy = this.shared.currentUser.showContact;
  }

  ionViewDidLeave() {
    this.options = {
      bio: false,
      email: false,
      privacy: false,
      security: false
    }
  }

  openConfig(group: string) {
    this.options[group] = !this.options[group];
  }

  updateBio() {
    this.userService.updateBio(this.newBio, this.shared.currentUser.token).subscribe(
      success => {
        this.shared.setUser(success.user);
        this.shared.presentAlert(success.message);
        this.newBio = success.user.about;
        this.openConfig('bio');
      }
    )
  }

  updatePrivacy() {
    if (this.privacy !== this.shared.currentUser.showContact) {
      this.userService.updatePrivacy(this.privacy, this.shared.currentUser.token).subscribe(
        success => {
          this.shared.setUser(success.user);
          this.shared.presentAlert(success.message);
          this.privacy = success.user.showContact;
          this.openConfig('privacy');
        }
      )
    } else {
      this.openConfig('privacy');
    }
  }

  updatePass() {
    if (!this.security.oldPass) {
      this.shared.presentAlert('Informe a senha antiga');
      return false;
    }

    if (!this.security.newPass) {
      this.shared.presentAlert('Informe a senha');

      return false;
    }

    if (!this.security.confirmNewPass) {
      this.shared.presentAlert('Confirme a senha');
      return false;
    }

    if (this.security.newPass !== this.security.confirmNewPass) {
      this.shared.presentAlert('As senhas não conferem');
      return false;
    }

    this.userService.updatePass(this.security, this.shared.currentUser.token).subscribe(
      success => {
        this.shared.presentAlert(success.message);
        this.security = {
          oldPass: '',
          newPass: '',
          confirmNewPass: ''
        }
        this.openConfig('security');
      }
    )
  }

}

interface ChangePass {
  oldPass: string;
  newPass: string;
  confirmNewPass: string;
}
