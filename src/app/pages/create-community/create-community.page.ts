import { Component, OnInit } from '@angular/core';

import { SharedService } from '../../services/shared.service';
import { CommunityService } from '../../services/community.service';

@Component({
  selector: 'app-create-community',
  templateUrl: './create-community.page.html',
  styleUrls: ['./create-community.page.scss'],
})
export class CreateCommunityPage implements OnInit {
  community = {
    name: '',
    email: '',
    phone: '',
    description: '',
    // address: {
    //   cep: '',
    //   logradouro: '',
    //   numero: '',
    //   complemento: '',
    //   bairro: '',
    //   localidade: '',
    //   uf: ''
    // }
  }
  searchAlert = false;

  constructor(
    public shared: SharedService,
    private communityService: CommunityService
  ) { }

  ngOnInit() {
  }

  onSubmit() {
    this.communityService.create(this.community, this.shared.currentUser.token).subscribe(
      success => {
        this.shared.presentAlert(success.message);
        this.community = {
          name: '',
          email: '',
          phone: '',
          description: '',
          // address: {
          //   cep: '',
          //   logradouro: '',
          //   numero: '',
          //   complemento: '',
          //   bairro: '',
          //   localidade: '',
          //   uf: ''
          // }
        }
        this.shared.navigate(`/community/${success.community._id}`);
      },
      error => {
        this.shared.presentError(error)
      }
    )
  }

  // findCep() {
  //   if (this.community.address.cep.length === 8 || this.community.address.cep.length === 9) {
  //     this.searchAlert = true
  //     this.community.address.logradouro = 'Buscando...';
  //     this.community.address.bairro = 'Buscando...';
  //     this.community.address.localidade = 'Buscando...';
  //     this.community.address.uf = 'Buscando...';
  //     this.shared.findCep(this.community.address.cep).subscribe(
  //       success => {
  //         this.community.address.logradouro = success.logradouro;
  //         this.community.address.bairro = success.bairro;
  //         this.community.address.localidade = success.localidade;
  //         this.community.address.uf = success.uf;
  //         this.searchAlert = false;
  //       },
  //       () => {
  //         this.community.address.logradouro = '';
  //         this.community.address.bairro = '';
  //         this.community.address.localidade = '';
  //         this.community.address.uf = '';
  //         this.searchAlert = false;
  //       }
  //     );
  //   }
  // }

}
