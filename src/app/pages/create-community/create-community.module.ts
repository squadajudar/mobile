import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateCommunityPageRoutingModule } from './create-community-routing.module';

import { CreateCommunityPage } from './create-community.page';

import { HeaderComponent } from '../../components/header/header.component';

import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CreateCommunityPageRoutingModule,
    BrMaskerModule
  ],
  declarations: [CreateCommunityPage, HeaderComponent]
})
export class CreateCommunityPageModule { }
