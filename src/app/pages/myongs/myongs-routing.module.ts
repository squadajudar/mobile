import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyongsPage } from './myongs.page';

const routes: Routes = [
  {
    path: '',
    component: MyongsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyongsPageRoutingModule {}
