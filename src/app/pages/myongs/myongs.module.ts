import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyongsPageRoutingModule } from './myongs-routing.module';

import { MyongsPage } from './myongs.page';

import { HeaderComponent } from '../../components/header/header.component';
import { OngComponent } from '../../components/ong/ong.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MyongsPageRoutingModule
  ],
  declarations: [MyongsPage, HeaderComponent, OngComponent]
})
export class MyongsPageModule { }
