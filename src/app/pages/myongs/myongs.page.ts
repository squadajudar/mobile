import { Component, OnInit } from '@angular/core';

import { SharedService } from '../../services/shared.service';
import { OngService } from '../../services/ong.service';

@Component({
  selector: 'app-myongs',
  templateUrl: './myongs.page.html',
  styleUrls: ['./myongs.page.scss'],
})
export class MyongsPage implements OnInit {
  ongs: any;
  filteredOngs: any
  search: any;

  constructor(
    public shared: SharedService,
    private ongService: OngService
  ) { }

  ngOnInit() { }

  ionViewWillEnter() {
    this.ongService.getMyOngsList(this.shared.currentUser.token).subscribe(
      success => {
        this.ongs = success;
        this.filteredOngs = success;
      }
    );
  }

  filterOscs() {
    this.filteredOngs = this.ongs.filter(
      osc => osc.name.toLowerCase().includes(this.search.toLowerCase())
    );
  }

}
