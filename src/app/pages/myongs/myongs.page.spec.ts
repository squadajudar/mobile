import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyongsPage } from './myongs.page';

describe('MyongsPage', () => {
  let component: MyongsPage;
  let fixture: ComponentFixture<MyongsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyongsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyongsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
