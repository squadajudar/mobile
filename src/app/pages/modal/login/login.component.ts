import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

import { SharedService } from '../../../services/shared.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;

  constructor(
    public shared: SharedService,
    public modalController: ModalController
  ) { }

  ngOnInit() { }

  async onSubmitLogin() {
    const loading = await this.shared.presentLoading('Verificando dados...');
    loading.present();

    this.shared.login(this.email, this.password).subscribe(
      success => {
        this.shared.setUser(success);
        this.email = null;
        this.password = null;
        this.modalController.dismiss();
        loading.dismiss();
      },
      response => {
        loading.dismiss();
        this.shared.presentError(response);
      }
    );
  }

}
