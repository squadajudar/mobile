export const environment = {
  production: true,
  api: 'https://esfredebeta.herokuapp.com',
  // api: 'http://192.168.18.3:3333',
  cep: 'https://viacep.com.br',
  map: 'https://nominatim.openstreetmap.org'
};
